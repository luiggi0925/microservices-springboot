package edu.home.ltmj.serviceprovider.component;

import org.springframework.stereotype.Component;

@Component("myComponent")
public class MyComponentImpl implements MyComponent {

//	@Override
//	public String sayHello(String name) {
//		return String.format("Hello %s from Apache Camel", name);
//	}

	@Override
	public void sayHello() {
		System.out.println("Hello from Apache Camel");
	}
}
