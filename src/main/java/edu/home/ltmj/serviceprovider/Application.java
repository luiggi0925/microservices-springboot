package edu.home.ltmj.serviceprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("edu.home.ltmj.serviceprovider")
public class Application {

	public static void main(String[] args) {
//		DefaultMuleContextFactory muleContextFactory = new DefaultMuleContextFactory();
//		SpringXmlConfigurationBuilder configBuilder = new SpringXmlConfigurationBuilder("mule-config.xml");
//		muleContext = muleContextFactory.createMuleContext(configBuilder);
		SpringApplication.run(Application.class, args);
	}
}
