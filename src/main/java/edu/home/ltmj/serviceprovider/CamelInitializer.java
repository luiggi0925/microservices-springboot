package edu.home.ltmj.serviceprovider;

import org.apache.camel.CamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import edu.home.ltmj.serviceprovider.component.MyComponent;
import edu.home.ltmj.serviceprovider.component.MyComponentImpl;

@Configuration
public class CamelInitializer {

	@Autowired
	private CamelContext camelContext;

	@Bean
	public MyComponent createMyComponent() {
		return new MyComponentImpl();
	}
}
