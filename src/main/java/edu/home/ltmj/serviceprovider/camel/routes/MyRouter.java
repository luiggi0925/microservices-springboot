package edu.home.ltmj.serviceprovider.camel.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class MyRouter extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("quartz2://cronExample?cron=0+0/1+*+*+*+?")
			.to("bean:myComponent?method=sayHello");
	}
}
